// make factory a singleton so that onece configured it can be easily used
const singletonFactory = function(documentRoot){
	htmlElementFactory = function(htmlString){
	  var t = documentRoot.createElement('template');
	  t.innerHTML = htmlString.trim();

	  return t.content.firstChild;
	}

	return htmlElementFactory;
}

export let htmlElementFactory = singletonFactory
