import css from "./element-selection.service.css";

export const elementSelectionService = function(documentBody, StepService){
	let currentTarget

	let mouseMoveListener = (event) => {
		if(event.ctrlKey){
			markTarget(event.target);
		} else {
			markTarget();
		}
	}

	let markTarget = (newTarget) => {
		if(currentTarget){
			currentTarget.className = currentTarget.className.replace("toBeSelected", "");
		}

		if(newTarget){
			newTarget.className = (newTarget.className + " toBeSelected").trim();
		}

		currentTarget = newTarget;
	}


	let mouseClickListener = (event) => {
		if(event.ctrlKey){
			StepService.addElementStep(event.target)
		}
	}

	documentBody.addEventListener('click', mouseClickListener)
	documentBody.addEventListener('mousemove', mouseMoveListener)
}