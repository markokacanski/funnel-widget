import {elementSelectionService} from './element-selection.service.js';

test('attach mouse move listener to document body', ()=>{
	let documentBody = {
		addEventListener: jest.fn()
	}

	let ESS = new elementSelectionService(documentBody);

	expect(documentBody.addEventListener.mock.calls[1][0]).toBe("mousemove");
	expect(typeof documentBody.addEventListener.mock.calls[1][1]).toBe("function");
})

test('attach click listener to document body', ()=>{
	let documentBody = {
		addEventListener: jest.fn()
	}
	let ESS = new elementSelectionService(documentBody);
	
	expect(documentBody.addEventListener.mock.calls[0][0]).toBe("click");
	expect(typeof documentBody.addEventListener.mock.calls[0][1]).toBe("function");
})

test('send element that was clicked if ctrl is held down', ()=>{
	let StepService = {
		addElementStep: jest.fn()
	}

	let documentBody = {
		addEventListener: jest.fn()
	}

	let event = {
		ctrlKey: true
	}

	let ESS = new elementSelectionService(documentBody, StepService)

	let onClickListener = documentBody.addEventListener.mock.calls[0][1]

	onClickListener(event)
	expect(StepService.addElementStep.mock.calls.length).toBe(1)
})

test('do NOT send elment that was clicked if ctrl is NOT held down', ()=>{
	let StepService = {
		addElementStep: jest.fn()
	}

	let documentBody = {
		addEventListener: jest.fn()
	}

	let event = {
		ctrlKey: false
	}

	let ESS = new elementSelectionService(documentBody, StepService)

	let onClickListener = documentBody.addEventListener.mock.calls[0][1]
	onClickListener(event)

	expect(StepService.addElementStep.mock.calls.length).toBe(0)
})

test("add and remove backtround when ctrl is pressed while moving mouse", ()=>{
	let StepService = {
	}

	let documentBody = {
		addEventListener: jest.fn()
	}

	let event1 = {
		ctrlKey: true,
		target: {
			className: ""
		}
	}

	let event2 = {
		ctrlKey: true,
		target: {
			className: ""
		}
	}

	let ESS = new elementSelectionService(documentBody, StepService)

	let onMoveListener = documentBody.addEventListener.mock.calls[1][1]
	onMoveListener(event1)
	onMoveListener(event2)

	expect(event1.target.className).not.toBe("toBeSelected")
	expect(event2.target.className).toBe("toBeSelected")
})

test("do not add background if already there", ()=>{
	let StepService = {
	}

	let documentBody = {
		addEventListener: jest.fn()
	}

	let event = {
		ctrlKey: true,
		target: {
			className: ""
		}
	}

	let ESS = new elementSelectionService(documentBody, StepService)

	let onMoveListener = documentBody.addEventListener.mock.calls[1][1]
	//move once to add it
	onMoveListener(event)
	//but don't add it the second time
	onMoveListener(event)

	expect(event.target.className).toBe("toBeSelected")
})