import {urlSelectionService} from './url-selection.service.js';

test('attach ctrl p listener to document body', ()=>{
	let documentBody = {
		addEventListener: jest.fn()
	}

	let stepService = {
		addUrlStep: jest.fn()
	}

	let window = {
		location: {
			href: "lol"
		}
	}

	let USS = new urlSelectionService(documentBody, stepService, window);

	expect(documentBody.addEventListener.mock.calls[0][0]).toBe("keydown");
	expect(typeof documentBody.addEventListener.mock.calls[0][1]).toBe("function");

	let event = {
		altKey: true,
		keyCode: 80
	}

	let onClickListener = documentBody.addEventListener.mock.calls[0][1]
	onClickListener(event)

	expect(stepService.addUrlStep).toHaveBeenCalled()
})