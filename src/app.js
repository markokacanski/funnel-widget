import "./app.css";

export default function App(
	StepsService, 
	AdminTunnel, 
	Document, 
	HtmlElementFactory,
	ElementSelectionService,
	ElementStepFactory,
	StepPersistanceService,
	Storage,
	UrlStepFactory,
	UrlSelectionService,
	window
){
	let secureCode = window.location.search.match(/funnelcreatorcode=(.*)/)


	if(!secureCode){
		secureCode = Storage.getItem('funnelcreatorcode')
	} else {
		secureCode = secureCode[1]
	}

	// obviously this is not secure
	// but in real life scenario we would use expiring public/private key pair 
	// and check validity on the backend before proceeding
	if(!secureCode || secureCode != "12345"){
		return null;
	}



	Storage.setItem('funnelcreatorcode', secureCode)

	let elementFactory = new HtmlElementFactory(Document);

	let template = elementFactory(`
		<div class="funnel-widget-1234">
			<div class="funnel-header">
				<p>New Funnel <span class="step-counter">0 steps</span> <button class="close-funnel">❌</button></p>
			</div>
			<div class="funnel-info">
				<p>Navigate through your site and add the steps that will make up the funnel</p>
			</div>
			<ol class="funnel-steps-box">
				<li class="instructions">
					<p>alt + p to add current url</p>
					<p>ctrl + click to add element</p>
				</li>
			</ol>
			<div class="funnel-actions">
				<button class="submit-funnel">Submit funnel</button>
			</div>
		</div>
	`);

	let stepListElement = template.querySelector(".funnel-steps-box");
	let stepCounter = template.querySelector(".step-counter");
	let submitButton = template.querySelector(".submit-funnel");



	let closeFunnelCreator = () => {
		Document.body.removeChild(template)
		Storage.clear();
		// this is also a great place to let the backend know that the secure code
		// can be considered expired
	}

	template.querySelector(".close-funnel").onclick = closeFunnelCreator


	let stepPersistanceService = new StepPersistanceService(Storage)
	let urlStepFactory = new UrlStepFactory(elementFactory)

	submitButton.onclick = ()=>{
		window.xcom.send('FUNNEL_ADDED', stepPersistanceService.retreive())
		closeFunnelCreator();
	}

	let elementStepFactory = new ElementStepFactory(elementFactory);
	let stepsService = new StepsService(
		stepPersistanceService, 
		elementStepFactory, 
		urlStepFactory,
		stepListElement,
		elementFactory,
		stepCounter
	);

	let elementSelectionService = new ElementSelectionService(Document.body, stepsService);
	let urlSelectionService = new UrlSelectionService(Document.body, stepsService, window)


	Document.body.appendChild(template);
}