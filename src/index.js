import {htmlElementFactory} from './utils/html-element.factory.js'
import App from "./app.js"
import {elementSelectionService} from './element-selection/element-selection.service.js'
import {StepsService} from './steps/steps.service.js'
import {ElementStepFactory} from './steps/element-step.factory.js'
import {StepPersistanceService} from './steps/step-persistance.service.js'
import {UrlStepFactory} from './steps/url-step.factory.js'
import {urlSelectionService} from './element-selection/url-selection.service.js'
import './xcom.js'

App(
	StepsService, 
	null, 
	document, 
	htmlElementFactory, 
	elementSelectionService, 
	ElementStepFactory,
	StepPersistanceService,
	window.localStorage,
	UrlStepFactory,
	urlSelectionService,
	window
)