import App from "./app.js";

test.skip("app starts with appending stuff to document body", ()=>{
	let document = {
		body: {
			appendChild: jest.fn()
		}
	};

	let stepsService = jest.fn();

	let elementSelectionService = jest.fn()

	let htmlElementFactory = jest.fn(()=>{
		return () => {
			return {
				querySelector: jest.fn()
			}
		}
	})

	let ElementStepFactory = jest.fn()
	let UrlStepFactory = jest.fn()
	let StepPersistanceService = jest.fn()

	App(
		stepsService,
		null, 
		document, 
		htmlElementFactory, 
		elementSelectionService, 
		ElementStepFactory, 
		StepPersistanceService, 
		//it insists that this is not a construcotr, even though it works just above with ElementStepFactory
		UrlStepFactory 
	);

	expect(document.body.appendChild.mock.calls.length).toBe(1)
})