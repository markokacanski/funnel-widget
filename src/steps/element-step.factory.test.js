import {ElementStepFactory} from "./element-step.factory.js"

let esf = new ElementStepFactory();

test("it builds a step from element", () =>{
	let element = {
		nodeName: "DIV"
	}

	let funnelStep = esf.build(element);

	expect(funnelStep.nodeName).toBe(element.nodeName)
})

test("it renders a html strimg from element", ()=>{
	let element = {
		nodeName: "DIV"
	}

	let funnelStep = esf.build(element);
	let renderedString = funnelStep.render()

	expect(renderedString).toMatch(/<option value="viewed">Viewed<\/option>/);
	expect(renderedString).toMatch(/<option value="clicked">Clicked<\/option>/);
	expect(renderedString).not.toMatch(/<option value="keyed">Key pressed or filled<\/option>/);
	expect(renderedString).not.toMatch(/<option value="focused">Focus<\/option>/);
	expect(renderedString).not.toMatch(/<option value="changed">Change<\/option>/);

})

test("it renders a special html string from text input element", ()=>{
	let element = {
		nodeName: "INPUT",
		type: "text"
	}

	let funnelStep = esf.build(element);
	let renderedString = funnelStep.render()

	expect(renderedString).toMatch(/<option value="viewed">Viewed<\/option>/);
	expect(renderedString).toMatch(/<option value="clicked">Clicked<\/option>/);
	expect(renderedString).toMatch(/<option value="keyed">Key pressed or filled<\/option>/);
	expect(renderedString).toMatch(/<option value="focused">Focus<\/option>/);
	expect(renderedString).not.toMatch(/<option value="changed">Change<\/option>/);

})


test("it renders even more special html string from changable elements", () => {
	let element = {
		nodeName: "INPUT",
		type: "radio"
	}

	let funnelStep = esf.build(element);
	let renderedString = funnelStep.render()

	expect(renderedString).toMatch(/<option value="viewed">Viewed<\/option>/);
	expect(renderedString).toMatch(/<option value="clicked">Clicked<\/option>/);
	expect(renderedString).not.toMatch(/<option value="keyed">Key pressed or filled<\/option>/);
	expect(renderedString).not.toMatch(/<option value="focused">Focus<\/option>/);
	expect(renderedString).toMatch(/<option value="changed">Change<\/option>/);
})

test("it restores element from storage", ()=>{
	let storedElement = {
		nodeName: "INPUT",
		type: "text",
		stepType: "textInput"
	}

	let funnelStep = esf.restore(storedElement);
	let renderedString = funnelStep.render();

	
	expect(renderedString).toMatch(/<option value="viewed">Viewed<\/option>/);
	expect(renderedString).toMatch(/<option value="clicked">Clicked<\/option>/);
	expect(renderedString).toMatch(/<option value="keyed">Key pressed or filled<\/option>/);
	expect(renderedString).toMatch(/<option value="focused">Focus<\/option>/);
	expect(renderedString).not.toMatch(/<option value="changed">Change<\/option>/);
})