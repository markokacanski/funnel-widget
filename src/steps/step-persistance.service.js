let stepsSoFar = []

export const StepPersistanceService = function(localstorage){

	this.save = (element) => {
		stepsSoFar.push(element)
		localstorage.setItem("funnel-widget-steps", JSON.stringify(stepsSoFar))
	}

	this.retreive = () => {
		return JSON.parse(localstorage.getItem("funnel-widget-steps"))
	}

	this.update = ()=>{
		localstorage.setItem("funnel-widget-steps", JSON.stringify(stepsSoFar))
	}

	this.deleteStep = (step)=>{
		stepsSoFar.splice(stepsSoFar.indexOf(step), 1);
		this.update();
	}
}