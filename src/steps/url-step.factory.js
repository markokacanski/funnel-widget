export const UrlStepFactory = function(htmlElementFactory){
	this.build = (url) => {

		let funnelStep = {
			url: url,
			stepType: "url",
			domElement: htmlElementFactory(`
				<li class="element-step">
					<div class="leading-line"></div>
					<p>User visits the page:</p>
					<p>
						${url}
					</p>						
						<button class="delete-step">❌</button>
				</li>
			`)
		}

		funnelStep.onStepDeleted = (fn) => {
			funnelStep.domElement.querySelector(".delete-step").addEventListener("click", fn)
		}

		return funnelStep
	}

	this.restore = (storageElement) => {
		return this.build(storageElement.url)
	}
}