import {UrlStepFactory} from "./url-step.factory.js"

let usf = new UrlStepFactory()

test("it builds an url step", ()=>{
	let url="www.lmao.com"

	let funnelStep = usf.build(url);
	let renderedString = funnelStep.render()

	expect(funnelStep.url).toBe(url)
	expect(renderedString).toMatch(url)
})

test("it restores an url step", ()=>{
	let storedElement = {
		stepType: "url",
		url: "www.lol.com"
	}

	let funnelStep = usf.restore(storedElement)
	let renderedString = funnelStep.render()

	expect(renderedString).toMatch(storedElement.url)
})