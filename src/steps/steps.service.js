let singleton;

export const StepsService = function(
	stepPersistanceService,
	elementStepFactory, 
	urlStepFactory, 
	stepListElement,
	htmlElementFactory,
	stepCounter
){
	if(singleton){
		return singleton
	}

	singleton = this;

	let stepCount = 0;

	let instrucitonElement = stepListElement.querySelector("li.instructions")

	let updateStepCounter = () => {
		stepCount += 1;
		stepCounter.innerHTML = `${stepCount} steps`
	}

	let existingSteps = stepPersistanceService.retreive()

	if(existingSteps){
		existingSteps.forEach((step) => {
			let funnelStep;

			if(step.stepType == "url"){
				funnelStep = urlStepFactory.restore(step)

				funnelStep.onStepDeleted(()=>{
					stepPersistanceService.deleteStep(funnelStep)
					stepListElement.removeChild(funnelStep.domElement)
				})

			} else {
				funnelStep = elementStepFactory.restore(step)

				funnelStep.onActionChanged(()=>{
					stepPersistanceService.update();
				})

				funnelStep.onStepDeleted(()=>{
					stepPersistanceService.deleteStep(funnelStep)
					stepListElement.removeChild(funnelStep.domElement)
				})
			}

			stepPersistanceService.save(funnelStep)
			stepListElement.insertBefore(funnelStep.domElement, instrucitonElement)
			updateStepCounter()
		})

	}

	this.addElementStep = (DOMElement) =>{
		let funnelStep = elementStepFactory.build(DOMElement);
		stepPersistanceService.save(funnelStep);

		funnelStep.onActionChanged((event)=>{
			stepPersistanceService.update();
		})

		funnelStep.onStepDeleted(()=>{
			stepPersistanceService.deleteStep(funnelStep)
			stepListElement.removeChild(funnelStep.domElement)
		})

		stepListElement.insertBefore(funnelStep.domElement, instrucitonElement)
		updateStepCounter()
	}

	this.addUrlStep = (url) => {
		let funnelStep = urlStepFactory.build(url);
		stepPersistanceService.save(funnelStep);

		funnelStep.onStepDeleted(()=>{
			stepPersistanceService.deleteStep(funnelStep)
			stepListElement.removeChild(funnelStep.domElement)
		})
		
		stepListElement.insertBefore(funnelStep.domElement, instrucitonElement)
		updateStepCounter()
	}
}

