import "./element-step.css"

export const ElementStepFactory = function(htmlElementFactory){

	this.build = (htmlElement) => {
		let options = regularElementOptions;
		let stepType = "regular"

		if(isKeypressElement(htmlElement)){
			options += keypressElementOptions;
			stepType = "changable"
		}

		if(isChangableElement(htmlElement)){
			options += changableElementOptions;
			stepType = "textInput"
		}

		let funnelStep = {
			nodeName: htmlElement.nodeName,
			type: htmlElement.type,
			action: "viewed",
			stepType: stepType,
			domElement: htmlElementFactory(`
				<li class="element-step">
					<div class="leading-line"></div>
					<p>User interacts with element:</p>
					<p>
						${htmlElement.nodeName} 

						<label>Action:</label>

						<select>
							${options}
						</select>

					</p>
						<button class="delete-step">❌</button>
					
					
				</li>
			`)
		}

		funnelStep.onActionChanged = (fn) => {
			funnelStep.domElement.querySelector("select").addEventListener('change', fn);
		}

		funnelStep.onStepDeleted = (fn) => {
			funnelStep.domElement.querySelector(".delete-step").addEventListener('click', fn)
		}


		funnelStep.onActionChanged((event)=>{
			funnelStep.action = event.target.value
		})

		funnelStep.setAction = (action) => {
			funnelStep.action = action;
			funnelStep.domElement.querySelector(`option[value=${action}]`).selected="selected"
		}

		return funnelStep
	}

	this.restore = (storageElement) => {
		let restoredElement = this.build(storageElement)
		restoredElement.setAction(storageElement.action)

		return restoredElement;
	}
}

let isKeypressElement = (htmlElement) => {
	let isTextInput = htmlElement.nodeName == 'INPUT' && (
		htmlElement.type == "color" ||
		htmlElement.type == "date" ||
		htmlElement.type == "datetime-local" ||
		htmlElement.type == "email" ||
		htmlElement.type == "file" ||
		htmlElement.type == "image" ||
		htmlElement.type == "month" ||
		htmlElement.type == "number" ||
		htmlElement.type == "password" ||
		htmlElement.type == "tel" ||
		htmlElement.type == "text" ||
		htmlElement.type == "time" ||
		htmlElement.type == "url" ||
		htmlElement.type == "week"
	);
	
	let isArea = htmlElement.nodeName == 'TEXTAREA';

	return isTextInput || isArea;
}

let isChangableElement = (htmlElement) => {
	let isChangableInput = htmlElement.nodeName == 'INPUT' && (
		htmlElement.type == 'checkbox' ||
		htmlElement.type == 'radio'
	)
	let isSelect = htmlElement.nodeName == 'SELECT'

	return isChangableInput || isSelect;
}

let regularElementOptions = `
	<option value="viewed">Viewed</option>
	<option value="clicked">Clicked</option>
`

let keypressElementOptions = `
	<option value="keyed">Key pressed or filled</option>
	<option value="focused">Focus</option>
`

let changableElementOptions = `
	<option value="changed">Change</option>
`