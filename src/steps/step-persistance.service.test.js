import {StepPersistanceService} from "./step-persistance.service.js"

let localstorage = {
	setItem: jest.fn(),
	getItem: jest.fn(()=>{
		return "[{\"name\":\"element1\"},{\"name\":\"element2\"},{\"name\":\"element3\"},{\"name\":\"element4\"}]"
	})
}

let sps = new StepPersistanceService(localstorage)

test("it should save a list of steps to the localstorage", () => {
	let elements = [
		{name: "element1"},
		{name: "element2"},
		{name: "element3"},
		{name: "element4"}
	]

	elements.forEach((element) => {
		sps.save(element)
	})

	expect(localstorage.setItem).toHaveBeenCalledTimes(4);
	expect(localstorage.setItem).toHaveBeenCalledWith( "funnel-widget-steps", "[{\"name\":\"element1\"},{\"name\":\"element2\"},{\"name\":\"element3\"},{\"name\":\"element4\"}]")
})


test("it should retreive a list of steps from the localstorage", ()=>{
	let existingElements = sps.retreive();


	expect(localstorage.getItem).toHaveBeenCalledWith("funnel-widget-steps");
	expect(Array.isArray(existingElements)).toBe(true);
})