import {StepsService} from "./steps.service.js"

let stepPersistanceService = {
	save: jest.fn(),
	retreive: jest.fn(()=>{
		return [
			{name: "DIV", stepType: "regular"},
			{name: "INPUT", type: "text", stepType:"textInput"},
			{name: "INPUT", type: "radio", stepType:"changeable"},
			{name: "www.lmao.com", stepType:"url"}
		]
	})
}

let elementStepFactory = {
	build: jest.fn(() => {
		return renderableElement
	}),

	restore: jest.fn(()=>{
		return renderableElement
	})
}

let urlStepFactory = {
	build: jest.fn(()=>{
		return renderableElement
	}),

	restore: jest.fn(()=>{
		return renderableElement
	})
}

let renderableElement = {
	render: jest.fn(()=>{
		return "it build the html string"
	})
}

let stepListElement = {
	insertBefore: jest.fn(()=>{
	}),

	querySelector: jest.fn(()=>{
		return "instruction element"
	})
}

let htmlElementFactory = jest.fn(()=>{
	return "it build the dom element"
})

let ss = new StepsService(
	stepPersistanceService, 
	elementStepFactory, 
	urlStepFactory, 
	stepListElement,
	htmlElementFactory
)

test("it adds element steps from the factory to th DOM and localstorage,", () => {
	let element =  {}

	ss.addElementStep(element)

	expect(elementStepFactory.build).toHaveBeenCalledWith(element);
	expect(stepPersistanceService.save).toHaveBeenCalledWith(renderableElement);
	expect(htmlElementFactory).toHaveBeenCalledWith("it build the html string")
	expect(stepListElement.insertBefore).toHaveBeenCalledWith("it build the dom element", "instruction element");
});


test("it adds url steps from a factory to the DOM and localstorage", ()=>{
	let url =  "www.lmao.com"

	ss.addUrlStep(url);

	expect(urlStepFactory.build).toHaveBeenCalledWith(url)
	expect(stepPersistanceService.save).toHaveBeenCalledWith(renderableElement);
	expect(htmlElementFactory).toHaveBeenCalledWith("it build the html string")
	expect(stepListElement.insertBefore).toHaveBeenCalledWith("it build the dom element", "instruction element");
})

test("it repopulates the step list from persistance service", ()=>{
	expect(stepPersistanceService.retreive).toHaveBeenCalled();
	expect(urlStepFactory.restore).toHaveBeenCalled();
	expect(elementStepFactory.restore).toHaveBeenCalled();
	expect(stepListElement.insertBefore).toHaveBeenCalled();
})