var path = require('path');

module.exports = {
  mode: 'development',
  entry: './src',
  
  module: {
  	rules: [
  		{
	      test: /\.css$/i,
	      use: ['style-loader', 'css-loader'],
	    },
  	]
  },

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'funnel-widget.bundle.js'
  }
};